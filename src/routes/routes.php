<?php

Route::group(['prefix' => 'user_channels'], function () {
    Route::get('/', 'Dreyko\Test\Controller\UserChannelController@index')->name('main_user_channels');
    Route::get('edit/{id}', 'Dreyko\Test\Controller\UserChannelController@editUserChannels')->name('edit_user_channels');
    Route::post('update/{id}', 'Dreyko\Test\Controller\UserChannelController@update')->name('update_user_channels');
});

Route::group(['prefix' => 'channels'], function () {
    Route::get('/', 'Dreyko\Test\Controller\ChannelController@index')->name('main_channel');
    Route::get('create', 'Dreyko\Test\Controller\ChannelController@create')->name('create_channel');
    Route::post('store', 'Dreyko\Test\Controller\ChannelController@store')->name('store_channel');
    Route::get('edit/{id}', 'Dreyko\Test\Controller\ChannelController@edit')->name('edit_channel');
    Route::post('update/{id}', 'Dreyko\Test\Controller\ChannelController@update')->name('update_channel');
});


