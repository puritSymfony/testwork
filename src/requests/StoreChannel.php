<?php

namespace Dreyko\Test\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreChannel extends FormRequest
{
    /**
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:10|regex:/^[a-zA-z]*$/',
        ];
    }
}
