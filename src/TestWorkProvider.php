<?php

namespace Dreyko\Test;

use Illuminate\Support\ServiceProvider;

class TestWorkProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ .
            '/database/migrations');
        include __DIR__.'/routes/routes.php';
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('Dreyko\Test\Controller\UserChannelController');
        $this->loadViewsFrom(__DIR__.'/views', 'example');
    }
}
