<?php

namespace Dreyko\Test\Controller;

use App\User;
use Dreyko\Test\Model\Channel;
use Illuminate\Contracts\View\Factory;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\View\View;

class UserChannelController extends Controller
{

    /**
     * @return Factory|View
     */
    public function index()
    {
        // laravel 5.3 и php 7.3 - выдаст ошибку compact(), поэтому вызову просто всех
        // $users = User::has('channels')->get();

        $users = User::all();
        return view('example::user_channel/index', compact('users'));
    }

    /**
     * @param $id
     * @return void
     */
    public function update($id)
    {
        $user = User::findOrFail($id);
        $checkedChannels = Input::get('channels');

        if($checkedChannels === null){
            $checkedChannels = [];
        }

        $user->channels()->sync($checkedChannels);

        return \Redirect::route('edit_user_channels', $id);
    }


    /**
     * @param $id
     * @return Factory|View
     */
    public function editUserChannels($id)
    {
        $user = User::findOrFail($id);
        $channels = Channel::all();

        foreach ($channels as $channel){
            $channel->subscribed = $user->channels->contains($channel);
        }

        return view('example::user_channel/edit', compact('user', 'channels'));
    }
}
