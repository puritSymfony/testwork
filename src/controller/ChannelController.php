<?php


namespace Dreyko\Test\Controller;


use App\Http\Controllers\Controller;
use Dreyko\Test\Model\Channel;
use Dreyko\Test\Requests\StoreChannel;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Input;

class ChannelController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {
        $channels = Channel::all();

        return view('example::channel/index', compact('channels'));
    }

    /**
     * @return Response
     */
    public function create()
    {
        return view('example::channel/create');
    }

    /**
     * @param StoreChannel $request
     * @return Response
     */
    public function store(StoreChannel $request)
    {
        $channel = new Channel();
        $channel->name = Input::get('name');
        $channel->save();

        return \Redirect::route('edit_channel', $channel->id);
    }

    /**
     * @param $id
     * @return Response
     */
    public function edit($id)
    {
        $channel = Channel::findOrFail($id);
        return view('example::channel/edit', compact('channel'));
    }

    /**
     * @param $id
     * @param StoreChannel $request
     * @return Response
     */
    public function update(StoreChannel $request, $id)
    {
        $channel = Channel::findOrFail($id);

        $channel->name = Input::get('name');
        $channel->save();

        return view('example::channel/edit', compact('channel'));
    }

}