# Requirements

requires TwigBridge 0.9,
laravelcollective/html 5.3 and
 Laravel 5.3.*

# Installation

Require this package with Composer

```bash
composer require dreyko/test --dev
```

# Configuration

1 ) Once Composer has installed or updated your packages you need to register package with Laravel itself. Open up config/app.php and find the providers key towards the bottom and add:

```php
'providers' => [
     ...
                Dreyko\Test\TestWorkProvider::class,
],
```
1.1) Make sure, that you have in your providers next lines
```php
'providers' => [
     ...
        TwigBridge\ServiceProvider::class,
        Collective\Html\HtmlServiceProvider::class,
     ...
],
```

2 ) Call migration
```php
php artisan migrate
```

3 ) After you need to add relation to your User model (app/User.php)

```php
public function channels()
    {
        return $this->belongsToMany('Dreyko\Test\Model\Channel');
    }
```

# Usage

To view, edit and make new channels
```php
channels/
```

To view users with channels 

```php
user_channels/
```

